#!/bin/bash


radius=$2
angle=$1

step=5

while [ $radius -gt 0 ]
do

r_loc=0

if [ $radius -ge $step ]
then
    ((radius-=step))
    r_loc=$step
else
    r_loc=$radius
    radius=0
fi

sleep 0.001

xdotool mousemove_relative --polar $angle $r_loc 

done
