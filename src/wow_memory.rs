extern crate cgmath;
extern crate byteorder;


use std::mem;
use std::mem::transmute;

use std::fs::File;
use std::io::SeekFrom;
use std::io::prelude::*;
use std::collections::HashMap;

use std::f32::consts;
use self::cgmath::Vector3;
use self::cgmath::EuclideanVector;
use byteorder::{ReadBytesExt, LittleEndian};

type GUID = u64;


//-------
#[derive(Debug)]
#[repr(C, packed)]
struct WoWObjectStruct {
    global_id: u64,
    descriptors: u64,
    unk1: u64,
    entity_type: u32,
    unk2: u64,
    entity_id: u32,
    dynamic_flags: u64
}

#[repr(u64)]
enum WoWOffsets {
    // Object Manager
    EntityList = 0x14E5580,
    FirstEntity = 0x18,
    NextEntity = 0x68,
    LocalPlayer = 0x1606AE0,
    // Object
    Level = 0x158,
    Coords = 0x1548,
    Guid = 0x50,
    TargetGuid = 0x16A25C8,
    UnitFlags = 0x17C,
    // Camera
    Camera = 0x16A2A10, 
    AxisX = 0x400,
    AxisY = 0x410,
    AxisZ = 0x420
}

#[repr(u64)]
enum WoWUnitBitflags {
    InCombat = 0xABCC771, //0x6DF37F6,
    IsAlive = 0x112E0BE8
}
//-------

macro_rules! readMem {
    ($self_:ident, $g:ident, $e:expr) => 
        {{
            $self_.process_memory.seek(SeekFrom::Start($e));
            match $self_.process_memory.$g::<LittleEndian>() {
                Ok(result) => result,
                Err(e) => panic!(e)
            }
        }}
}

trait UnitTrait {
    fn getPosition(&mut self) -> Vector3<f32>;
}

#[derive(Debug)]
enum WoWObjectType {
    Unit {is_player: bool, level: u64},
    Object
}

#[derive(Debug)]
pub struct WoWObject {
    pointer: u64,
    object_type: WoWObjectType
}

/*impl UnitTrait for WoWObject {
    pub fn getPosition(&mut self) -> Vector3<f32>{
        
    }
}*/


pub struct WoWObjectManager {
    objects: HashMap<GUID, WoWObject>,
    base_address: u64,
    process_memory: File
}

impl WoWObjectManager {
    pub fn new(pid: String, base_address: u64) -> Result<WoWObjectManager, String>{
        let mut objects: HashMap<GUID, WoWObject> = HashMap::new();
        let pm_file = File::open(format!("/proc/{}/mem", pid));
        let mut process_memory = 
            match pm_file {
                Ok(f) => f,
                Err(e) => 
                {
                    return Err(format!("failed to read process memory: {:?}", e).to_string());
                }
            }; 
        Ok(WoWObjectManager { 
            objects: objects,
            process_memory: process_memory,
            base_address: base_address
        })   
    }

    pub fn refresh(&mut self){
        self.objects.clear();
        // find first object
        let entity_list = readMem!(self, read_u64, self.base_address + WoWOffsets::EntityList as u64);
        let mut next_object = readMem!(self, read_u64, entity_list + WoWOffsets::FirstEntity as u64);
 
        let mut tmp_struct: *const WoWObjectStruct;
        let mut buffer : [u8; 48] = [0; 48];

        while next_object as u32 != 0 {
            self.process_memory.seek(SeekFrom::Start(next_object));
            self.process_memory.read(&mut buffer);
            tmp_struct = unsafe { transmute(buffer.as_ptr()) };
            let entity_type: u32;
            let descriptors: u64;
            unsafe {
                entity_type = (*tmp_struct).entity_type;
                descriptors = (*tmp_struct).descriptors;
            }
            match entity_type {
                4 | 3  => {
                    let level = readMem!(self, read_u64, descriptors + WoWOffsets::Level as u64); 
                    let guid: GUID = readMem!(self, read_u64, next_object + WoWOffsets::Guid as u64);
                    self.objects.insert(
                        guid, 
                        WoWObject {
                            pointer: next_object,
                            object_type: WoWObjectType::Unit { 
                                level: level, 
                                is_player: entity_type == 4 
                            }
                        }
                    );
                },
                _ => {}
            }
            self.process_memory.seek(SeekFrom::Start(next_object + WoWOffsets::NextEntity as u64));
            match self.process_memory.read_u64::<LittleEndian>() {
                Ok(addr) => next_object = addr,
                Err(_) => {break;}
            }
        }
    }

    pub fn get_local_player_guid(&mut self) -> GUID {
        let local_player = readMem!(self, read_u64, self.base_address + WoWOffsets::LocalPlayer as u64);
        readMem!(self, read_u64, local_player + WoWOffsets::Guid as u64)
    }

    pub fn get_target_guid(&mut self) -> GUID {
        readMem!(self, read_u64, self.base_address + WoWOffsets::TargetGuid as u64)
    }

    pub fn get_object(&self, guid: GUID) -> &WoWObject {
        self.objects.get(&guid).unwrap()     
    }

    pub fn get_camera(&mut self) -> Vector3<f32> {
        let camera = readMem!(self, read_u64, self.base_address + WoWOffsets::Camera as u64);
        let (x, y, z) = (
            readMem!(self, read_f32, camera + WoWOffsets::AxisX as u64),
            readMem!(self, read_f32, camera + WoWOffsets::AxisY as u64),
            readMem!(self, read_f32, camera + WoWOffsets::AxisZ as u64)
        );
        Vector3 {x: x, y: y, z: z } 
    }

    pub fn get_object_position(&mut self, guid: GUID) -> Vector3<f32> {
        match self.objects.get(&guid) {
            Some(object) => {
               self.process_memory.seek(SeekFrom::Start(object.pointer + WoWOffsets::Coords as u64));
               Vector3 {
                   x: self.process_memory.read_f32::<LittleEndian>().unwrap(),
                   y: self.process_memory.read_f32::<LittleEndian>().unwrap(),
                   z: self.process_memory.read_f32::<LittleEndian>().unwrap()
               }
            },

            None => panic!("{} is not found", guid)
        }
    }

    pub fn get_unit_flags(&mut self, guid: GUID) -> (bool, bool) {
        let object = self.objects.get(&guid).unwrap();
        let descriptors = readMem!(self, read_u64, object.pointer + 0x8);
        let unit_flags = readMem!(self, read_u64, descriptors + WoWOffsets::UnitFlags as u64);
        (
            unit_flags & WoWUnitBitflags::InCombat as u64 != 0,
            unit_flags & WoWUnitBitflags::IsAlive as u64 != 0
        )
    }

    pub fn get_vector(&mut self, guid1: GUID, guid2: GUID) -> Vector3<f32> {
        let pos1 = self.get_object_position(guid1);
        let pos2 = self.get_object_position(guid2);
        (pos2 - pos1).normalize()
    }
    
    #[inline(always)]
    fn angle(&self, (x1, y1): (f32, f32), (x2, y2): (f32, f32)) -> (bool, f32) {
        let a = (x1 * x2 + y1 * y2) / (x1 * x1 + y1 * y1).sqrt() / (x2 * x2 + y2 * y2).sqrt();
        let b = x1 * y2 - y1 * x2 > 0f32;
        (b, 180f32 * a.acos() / consts::PI)
    }

    fn is_above(&mut self, guid1: GUID, guid2: GUID) -> bool {
        let pos1 = self.get_object_position(guid1);
        let pos2 = self.get_object_position(guid2);
        pos1.z > pos2.z && pos1.y < pos2.y
    }

    pub fn angles(&mut self, guid1: GUID, guid2: GUID) -> ((bool, f32), (bool, f32)) {
        let camera_vec = self.get_camera();
        let target_vec = self.get_vector(guid1, guid2);
        (
            self.angle((camera_vec.x, camera_vec.y), (target_vec.x, target_vec.y)),
            {
                let (v1, v2) = self.angle((camera_vec.y, camera_vec.z), (camera_vec.y, target_vec.z));
                if self.is_above(guid1, guid2)
                { (v1, v2) }
                else
                { (!v1, v2) }
            },
        ) 
    }
    
    pub fn get_distance(&mut self, guid1: GUID, guid2: GUID) -> f32 {
        let pos1 = self.get_object_position(guid1);
        let pos2 = self.get_object_position(guid2);
        (pos2 - pos1).length()
    }

}






