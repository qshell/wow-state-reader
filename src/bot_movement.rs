use std::process::Command;

const PIXELS_PER_ANGLE: f32 = 4.4444270618f32;


pub trait MovementAction {
    fn look_at(&mut self, angle: f32, radius: f32) -> &Self;
    fn start_looking(&mut self) -> &Self;
    fn stop_looking(&mut self) -> &Self;
    fn start_moving(&mut self) -> &Self;
    fn stop_moving(&mut self) -> &Self;
}

pub struct MovementStateManager {
    pub is_moving: bool,
    pub is_looking: bool
}

impl MovementStateManager {
    pub fn new() -> MovementStateManager {
        MovementStateManager { is_moving: false, is_looking: false }
    }
}

impl MovementAction for MovementStateManager {
    fn start_looking(&mut self) -> &MovementStateManager {
        let result = Command::new("./scripts/mouse_down.sh")
            .arg(format!("{}", 1))
            .output()
            .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
        self.is_looking = true;
        self
    }           
   
    fn look_at(&mut self, angle: f32, radius: f32) -> &MovementStateManager {
        let result = Command::new("./scripts/move_camera_relative.sh")
            .arg(format!("{}", angle.round())).arg(format!("{}", (radius * PIXELS_PER_ANGLE).round() ))
            .output()
            .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
        self
    }

    fn stop_looking(&mut self) -> &MovementStateManager {
        let result = Command::new("./scripts/mouse_up.sh")
            .arg(format!("{}", 1))
            .output()
            .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
        self.is_looking = false;
        self
    }
    
    fn stop_moving(&mut self) -> &MovementStateManager {
        if self.is_moving {
            let result = Command::new("./scripts/mouse_up.sh")
                .arg(format!("{}", 3))
                .output()
                .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
            self.is_moving = false;
        }
        self
    } 

    fn start_moving(&mut self) -> &MovementStateManager {
        if self.is_looking {
            let result = Command::new("./scripts/mouse_down.sh")
                .arg(format!("{}", 3))
                .output()
                .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
            self.is_moving = true;
        }
        self
    } 
}

