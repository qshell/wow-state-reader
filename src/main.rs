extern crate rustc_serialize;
extern crate regex;
extern crate byteorder;

use std::sync::{RwLock, Arc};
use std::thread;
use std::fs::File;
use std::io::prelude::*;
use std::process::Command;

use rustc_serialize::hex::FromHex;
use regex::Regex;

mod wow_memory;
mod bot_movement;

use wow_memory::WoWObjectManager;
use bot_movement::{MovementStateManager, MovementAction};


const PROCESS_NAME : &'static str = "Wow-64.exe";


fn int_from_bytes(array: Vec<u8>) -> u64 {
    let mut res: u64 = 0;
    for b in array {
        res = (res << 8) + (b as u64);
    }
    res
}


fn main() {
    let mut m = MovementStateManager::new();
	let output = Command::new("pgrep")
     	.arg(PROCESS_NAME)
     	.output()
     	.unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
    
    if output.stdout.is_empty(){
        panic!("game process wasn't found");  
    }

    let pid = String::from_utf8(output.stdout).unwrap().replace("\n", "");
    let mut file = File::open(format!("/proc/{}/maps", pid)).unwrap();

    let mut s = String::new();
    file.read_to_string(& mut s);
    let re = Regex::new(& format!(r"([0-9a-f]+)-([0-9a-f]+).*{}", PROCESS_NAME)).unwrap();
    let cap = re.captures(& s).unwrap();
    let base_address = int_from_bytes((cap.at(1).unwrap().to_string() + "0").from_hex().unwrap()) >> 4;
    let lock = Arc::new(RwLock::new(WoWObjectManager::new(pid, base_address).unwrap()));
    let writer1 = lock.clone();
    {
        let mut  wowom = writer1.write().unwrap();
        wowom.refresh();
    }

    let writer2 = lock.clone();
    thread::spawn(move || {
        loop {
            thread::sleep_ms(100);
            let mut wowom = writer2.write().unwrap();
            wowom.refresh();
        }
    });
   
    //m.start_looking();
    //m.start_moving();

    loop {
        thread::sleep_ms(200);
        let mut wow_object_manager = writer1.write().unwrap(); 
        let player_guid = wow_object_manager.get_local_player_guid();
        let target_guid = wow_object_manager.get_target_guid();
        println!("{:?}", wow_object_manager.get_object(player_guid));
        println!("{:?}", wow_object_manager.get_object(target_guid));
        let (flat_hor, flat_ver) = wow_object_manager.angles(player_guid, target_guid);
        let distance = wow_object_manager.get_distance(player_guid, target_guid);
        let mut angle = if flat_ver.1 == 0f32 {  90f32 } else { 180f32 * (flat_hor.1 / flat_ver.1).atan() / 3.1415f32 };
        println!("{:?} : {:?}", flat_hor.0, flat_ver.0);       
        angle = match (flat_hor.0, flat_ver.0){
            (v1, v2) if v1 && !v2 => angle + 180f32,
            (v1, v2) if v1 && v2 => 360f32 - angle,
            (v1, v2) if !v1 && v2 => angle,
            (v1, v2) if !v1 && !v2 => 180f32 - angle,
            _ => 0f32
        };
        let radius = (flat_hor.1 * flat_hor.1 + flat_ver.1 * flat_ver.1).sqrt(); 
        println!("{:?} : {:?}", angle, radius);
        println!("{:?}", distance);
        //m.look_at(angle, radius);
        if distance <= 5f32 && m.is_moving {
            m.stop_moving();    
        }
        println!("player {:?}", wow_object_manager.get_unit_flags(player_guid));
        println!("targer {:?}", wow_object_manager.get_unit_flags(target_guid));
    }
}
